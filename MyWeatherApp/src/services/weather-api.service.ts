import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map, isEmpty } from 'rxjs/operators';
import { WeatherFilter } from '../models/weatherFilter';
import { isNullOrUndefined } from 'util';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class WeatherApiService {
  apiKeyParameter = '&appid=3a25eace17b06fbf19468876ab37df14';
  apiUrl = 'http://api.openweathermap.org/data/2.5';
  actualFilter: WeatherFilter;


  constructor(public http: Http, private storege: Storage) {
  }

  getCurrentByCityName(filter: WeatherFilter): Observable<WeatherFilter> {
    this.saveActualFilter(filter);
    const param = this.getParamFromFilter(filter);

    const url = this.apiUrl + '/weather?q=' + param + this.apiKeyParameter;
    const response = this.http.get(url).pipe(map(res => res.json()));
    return response;
  }

  getForecastByCityName(filter: WeatherFilter): Observable<any> {
    const param = this.getParamFromFilter(filter);

    const url = this.apiUrl + '/forecast?q=' + param + this.apiKeyParameter;
    const response = this.http.get(url).pipe(map(res => res.json()));
    return response;
  }

  private getParamFromFilter(filter: WeatherFilter): string {
    let param = '';
    if (!isNullOrUndefined(filter.CityName)  && filter.CityName !== '') {
      param = filter.CityName ;

      if (!isNullOrUndefined(filter.CountryCode) && filter.CountryCode !== '') {
        param += ',' + filter.CountryCode;
      }
    }
    return param;
  }

  getImageUrl(imageName: string): string {
    return 'http://openweathermap.org/img/w/' + imageName + '.png';
  }

  getCelsiusFromKelvin(kelvin: number): number {
    return kelvin - 273.15;
  }

  private saveActualFilter(filter: WeatherFilter) {
    this.actualFilter = new WeatherFilter();
    this.actualFilter.CityName = filter.CityName;
    this.actualFilter.CountryCode = filter.CountryCode;
  }

  getActualFilter(): WeatherFilter {
    return this.actualFilter;
  }

  getFavoriteWeather (): WeatherFilter {
    let filter = new WeatherFilter;
    this.storege.get('favoriteLocation').then((value) => {
      if (value != null) {
        const location = JSON.parse(value);
        filter.CityName = location.CityName;
        filter.CountryCode = location.CountryCode;
      } else {
        filter = null;
      }
    });
    return filter;
  }

  saveFavoriteWeather (filter: WeatherFilter) {
    this.storege.set('favoriteLocation', JSON.stringify(filter));
  }

  clearFavoriteWeather () {
    this.storege.clear();
  }
}
