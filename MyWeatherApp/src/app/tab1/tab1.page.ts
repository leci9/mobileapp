import { Component } from '@angular/core';
import { WeatherApiService } from './../../services/weather-api.service';
import { WeatherFilter } from '../../models/weatherFilter';
import { Observable, forkJoin } from 'rxjs';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  weatherData: any;
  filter = new WeatherFilter;

  constructor(private weatherApi: WeatherApiService, private storage: Storage) {

  }

  ionViewWillEnter() {
  }

  getForecast() {
    this.getWeatherData(this.filter);
  }

  getFavoriteForecast() {
    const filter = new WeatherFilter;
    this.storage.get('favoriteLocation').then((value) => {
      if (value != null) {
        const location = JSON.parse(value);
        filter.CityName = location.CityName;
        filter.CountryCode = location.CountryCode;

        this.getWeatherData(filter);
      }
    });
  }

  private getWeatherData(filter: WeatherFilter) {
    this.weatherApi.getCurrentByCityName(filter)
    .subscribe(data => {
      this.weatherData = data;
      console.log(this.weatherData);
      this.filter.CityName = '';
      this.filter.CountryCode = '';
    },
    err => {
      console.log(err);
    });
  }

  getImageUrl(imageName: string): string {
    return this.weatherApi.getImageUrl(imageName);
  }

  getCelsiusFromKelvin(kelvin: number): number {
    return this.weatherApi.getCelsiusFromKelvin(kelvin);
  }
}

