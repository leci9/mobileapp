import { Component } from '@angular/core';
import { WeatherApiService } from './../../services/weather-api.service';
import { WeatherFilter } from '../../models/weatherFilter';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  forecastrData: Array<any> = [];
  city: any = null;
  filter = new WeatherFilter;

  constructor(private weatherApi: WeatherApiService) {

  }

  ionViewWillEnter() {
    this.filter = this.weatherApi.getActualFilter();

    if (this.filter) {
      this.getForecastData(this.filter);
    }
  }

  private getForecastData(filter: WeatherFilter) {
    this.weatherApi.getForecastByCityName(filter)
    .subscribe(data => {
      this.city = data.city;
      this.forecastrData = data.list;
      console.log(data);
    },
    err => {
      console.log(err);
    });
  }

  getImageUrl(imageName: string): string {
    return this.weatherApi.getImageUrl(imageName);
  }

  getCelsiusFromKelvin(kelvin: number): number {
    return this.weatherApi.getCelsiusFromKelvin(kelvin);
  }
}
