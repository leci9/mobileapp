import { Component } from '@angular/core';
import { WeatherFilter } from '../../models/weatherFilter';
import { WeatherApiService } from './../../services/weather-api.service';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  filter: WeatherFilter;

  constructor(private weatherApi: WeatherApiService, public navController: NavController, private storage: Storage) {
    this.filter = new WeatherFilter;
    this.fillFilterFromStorage();
  }

  saveFavoriteLocation() {
    this.weatherApi.saveFavoriteWeather(this.filter);
  }

  clearFavoriteLocation() {
    this.weatherApi.clearFavoriteWeather();
    this.filter = new WeatherFilter;
  }

  fillFilterFromStorage() {
    this.storage.get('favoriteLocation').then((value) => {
      if (value != null) {
        const location = JSON.parse(value);
        this.filter.CityName = location.CityName;
        this.filter.CountryCode = location.CountryCode;
      }
    });
  }
}
